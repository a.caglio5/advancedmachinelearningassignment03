import numpy as np
from sklearn.model_selection import train_test_split
from tensorflow import keras
from assignment03.functions import model_definition, plot_history

"""Data preparation"""

# Model/data parameters
num_classes = 10
input_shape = (28, 28, 1)

# load the data and divide it into train/test split
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Scale images into the [0 1] range
x_train = x_train.astype("float32") / 255
x_test = x_test.astype("float32") / 255

# Make images with size (28,28,1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)

# Split data into train/validation set
x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.1, random_state=0)

print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_validation.shape[0], "validation samples")
print(x_test.shape[0], "test samples")

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_validation = keras.utils.to_categorical(y_validation, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

"""Model definition"""

model = model_definition(input_shape, num_classes)

model.summary()

"""Model Training"""

batch_size = 64
epochs = 80

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
model_history = model.fit(x_train, y_train,
                          batch_size=batch_size,
                          epochs=epochs,
                          verbose=1,
                          validation_data=(x_validation, y_validation))

# Plot loss and accuracy on training and validation set
x_plot = list(range(1, epochs + 1))
plot_history(x_plot, model_history)

"""Model Evaluation"""

score_train = model.evaluate(x_train, y_train, verbose=0)
print("Train loss:", score_train[0])
print("Train accuracy:", score_train[1])

score_validation = model.evaluate(x_validation, y_validation, verbose=0)
print("Validation loss:", score_validation[0])
print("Validation accuracy:", score_validation[1])

score_test = model.evaluate(x_test, y_test, verbose=0)
print("Test loss:", score_test[0])
print("Test accuracy:", score_test[1])
