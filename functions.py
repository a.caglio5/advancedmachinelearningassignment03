import matplotlib.pyplot as plt
from tensorflow import keras
from tensorflow.keras import layers


def model_definition(input_shape, num_classes):
    model = keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Conv2D(11, kernel_size=(2, 2), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(22, kernel_size=(2, 2), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(44, kernel_size=(2, 2), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten(),
            layers.Dropout(0.3),
            layers.Dense(num_classes, activation="softmax"),
        ]
    )

    return model


def plot_history(x_plot, model_history):
    plt.figure()
    plt.title('Model Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')

    plt.plot(x_plot, model_history.history['loss'])
    plt.plot(x_plot, model_history.history['val_loss'])
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.title('Model Accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')

    plt.plot(x_plot, model_history.history['accuracy'])
    plt.plot(x_plot, model_history.history['val_accuracy'])
    plt.legend(['Training', 'Validation'], loc='lower right')

    plt.show()
